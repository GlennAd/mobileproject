package com.example.cbk12_000.mobileproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;

import com.example.cbk12_000.mobileproject.Vector2D.Vector2d;
import com.example.cbk12_000.mobileproject.Vector2D.Vector2i;

import java.util.ArrayList;

/**
 * Custom view for gameplay
 */
public class GameView extends View {
    private Ball ball;

    private Paint paint;

    private ArrayList<Wall> walls;
    private ArrayList<Vector2i> obstacles;
    private Vector2i gameSize;
    private int width;
    private int height;
    private int lvl;

    private Vector2i camera;

    private Vector2i goalPoint;

    private long timeLastFrame;
    private long timeleft;

    private boolean finished;

    private ArrayList<Wall> collisions;

    private SharedPreferences preferences;

    private boolean levelLoaded = false;

    /**
     * Constructor for the GameView
     *
     * @param context The context the game view is in
     */
    public GameView(Context context) {
        super(context);
        init();
    }

    /**
     * Initialization function, called by constructor
     */
    private void init() {
        paint = new Paint();

        preferences = getContext().getSharedPreferences("Name", Context.MODE_PRIVATE);
        finished = false;
        Paint ballPaint = new Paint();
        int color = preferences.getInt("ballColor", 0);

        ballPaint.setColor(Option.transform(color));

        ball = new Ball(ballPaint);
        ball.radius = 10;

        camera = new Vector2i(0, 0);

        collisions = new ArrayList<>();

        timeleft = 200000;
        CountDownTimer mcountdowntimer = new CountDownTimer(timeleft, 1000) {
            @Override
            public void onTick(long l) {
                timeleft = l;
            }

            @Override
            public void onFinish() {

            }
        }.start();

        timeLastFrame = System.currentTimeMillis();
    }


    /**
     * Called when the view changes size. It sets the ball radius and build the level
     *
     * @param w Width of the view
     * @param h Height of the view
     * @param oldw Old width of the view
     * @param oldh Old height of the view
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        ball.radius = w / 50;

        width = w;
        height = h;

        buildLevel();
    }


    /**
     * Called by android to draw the scene. Uses the colors chosen by the player in Options
     *
     * @see Option
     * @param canvas The canvas we draw on
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int color = preferences.getInt("wallColor", 0);

        paint.setColor(Option.transform(color));

        for(Wall wall : walls)
        {
            canvas.drawRect(wall.startingPoint.x - camera.x, wall.startingPoint.y - camera.y, wall.endPoint.x - camera.x, wall.endPoint.y - camera.y, paint);
        }
        paint.setColor(Color.MAGENTA);
        for(Vector2i obstacle : obstacles)
        {
            canvas.drawCircle(obstacle.x - camera.x, obstacle.y - camera.y, width / 60.f, paint);
        }

        paint.setColor(Color.YELLOW);
        canvas.drawCircle(goalPoint.x - camera.x, goalPoint.y - camera.y, width / 60.f, paint);

        canvas.drawCircle(ball.pos.x - camera.x, ball.pos.y - camera.y, ball.radius, ball.paint);

        paint.setColor(Color.BLUE);
        paint.setTextSize(150);
        paint.setTextAlign(Paint.Align.LEFT);
        canvas.drawText(Long.toString(timeleft / 1000), 0, 0 + paint.getTextSize(), paint);

        invalidate();
    }

    /**
     * Update functions to update positions and do the physics calculations
     *
     * @param orientation Orientation of the phone, used to accelerate the ball
     */
    public void update(Vector2d orientation)
    {
        if(levelLoaded)
        {
                long currentTime = System.currentTimeMillis();
            long deltaTime = currentTime - timeLastFrame;

            if(deltaTime < 1000 / 60)
            {
                return;
            }
            timeLastFrame = currentTime;
            double dt = (double)deltaTime / 1000.0;

            ball.updateSpeed(orientation, dt);
            ball.updatePos(dt);

            collisions.clear();

            for(Vector2i obstacle : obstacles)
            {
                Vector2d diff = new Vector2d(Math.abs(obstacle.x) - Math.abs(ball.pos.x),Math.abs(obstacle.y) - Math.abs(ball.pos.y));
                double dist = diff.absSqrt();
                if(dist < (width / 60.0f) * (width / 60.0f) + ball.radius * ball.radius)
                {
                    respawn();
                }
            }

            Vector2d diff = new Vector2d(Math.abs(goalPoint.x - ball.pos.x),Math.abs(goalPoint.y -ball.pos.y));
            double dist = diff.absSqrt();
            if(dist < (width / 60.0f) * (width / 60.0f) + ball.radius * ball.radius)
            {
                finish();
            }

            for(Wall wall : walls)
            {
                if(wall.intersects(ball))
                {
                    collisions.add(wall);
                }
            }



            if(!collisions.isEmpty())
            {
                if(collisions.size() == 2)
                {
                    Wall wall0 = collisions.get(0);
                    Wall wall1 = collisions.get(1);

                    if((wall0.startingPoint.x == wall1.startingPoint.x &&
                    wall0.endPoint.x == wall1.endPoint.x) ||
                            (wall0.startingPoint.y == wall1.startingPoint.y &&
                            wall0.endPoint.y == wall1.endPoint.y))
                    {
                        collisions.clear();
                        collisions.add(new Wall(wall0.startingPoint, wall1.endPoint));
                    }
                }

                for(Wall collision : collisions)
                {
                    Vector2i circleDistance = new Vector2i();

                    circleDistance.x = Math.abs(ball.pos.x - (collision.startingPoint.x + (collision.width / 2)));
                    circleDistance.y = Math.abs(ball.pos.y - (collision.startingPoint.y + (collision.height / 2)));

                    double ballBounce = 0.6;
                    if(circleDistance.x <= collision.width / 2)
                    {
                        ball.speed.y = -ball.speed.y * ballBounce;
                        ball.pos.y = ball.oldPos.y;
                    }
                    else if(circleDistance.y <= collision.height / 2)
                    {
                        ball.speed.x = -ball.speed.x * ballBounce;
                        ball.pos.x = ball.oldPos.x;
                    }
                    else
                    {
                        ball.pos = ball.oldPos;

                        Vector2i corner = collision.startingPoint;

                        if(ball.pos.sub(new Vector2i(collision.endPoint.x, collision.startingPoint.y)).absSqrt() < corner.sub(ball.pos).absSqrt())
                        {
                            corner = new Vector2i(collision.endPoint.x, collision.startingPoint.y);
                        }
                        if(ball.pos.sub(new Vector2i(collision.startingPoint.x, collision.endPoint.y)).absSqrt() < corner.sub(ball.pos).absSqrt())
                        {
                             corner = new Vector2i(collision.startingPoint.x, collision.endPoint.y);
                         }
                         if(ball.pos.sub(new Vector2i(collision.endPoint.x, collision.endPoint.y)).absSqrt() < corner.sub(ball.pos).absSqrt())
                        {
                            corner = new Vector2i(collision.endPoint.x, collision.endPoint.y);
                        }

                        double x = ball.pos.x - corner.x;
                        double y = ball.pos.y - corner.y;

                        double c = -2 * (ball.speed.x * x + ball.speed.y * y) / (x * x + y * y);
                        if(!Double.isNaN(c))
                        {
                            ball.speed.x = (ball.speed.x + c * x) * ballBounce;
                            ball.speed.y = (ball.speed.y + c * y) * ballBounce;
                        }
                        else
                        {
                            ball.speed.x = 0;
                            ball.speed.y = 0;
                        }
                    }
                }
            }

            camera.x = ball.pos.x - width / 2;
            camera.y = ball.pos.y - height / 2;

            if(camera.x < 0)
            {
                camera.x = 0;
            }
            else if(camera.x + width > gameSize.x)
            {
                camera.x = gameSize.x - width;
            }

            if(camera.y < 0)
            {
                camera.y = 0;
            }
            else if(camera.y + height > gameSize.y)
            {
                camera.y = gameSize.y - height;
            }
        }
        else
        {
            timeLastFrame = System.currentTimeMillis();
        }
    }

    /**
     * Recreates the activity to restart the level
     */
    private void respawn()
    {
        GameActivity gameActivity = (GameActivity)getContext();

        gameActivity.recreate();
    }

    /**
     * Using a boolean the game checks that the level hasn't already been registered as completed
     * to prevent a bug, then registers the time and username into the highscore list
     * then boots the player to the menu
     */
    private void finish()
    {
        if(!finished) {
            finished = true;
            GameActivity gameActivity = (GameActivity) getContext();

            Database.getInstance().addScore(LevelList.getInstance().getNames()[lvl], preferences.getString("name", "Anon"), (int) timeleft);

            gameActivity.finish();
        }
    }

    /**
     * Builds the level using the information from the level files
     * Firstly gets the level from the levellist
     * Then creates a translation value to convert to what the size of the map should be
     * Using this value to convert the values, it then sets the starting position,
     * goal position and obstacle locations, and creates walls.
     * Also stores a game size value
     */
    private void buildLevel()
    {
        GameActivity gameActivity = (GameActivity)getContext();
        lvl = gameActivity.getIntent().getIntExtra("level", 0);

        Level level = LevelList.getInstance().getLevel(lvl);


        Vector2d screenTranslateValue = new Vector2d(width/50.f, height / 30.f);


        Log.i("Height",""+height);

        Vector2d startPos = screenTranslateValue.multiply(level.getStartPoint());

        obstacles = new ArrayList<>();

        for (int i = 0; i < level.getDangerPoints().size(); i++)
        {
            Vector2i obstacle = new Vector2i(level.getDangerPoints().get(i).multiply(screenTranslateValue));
            obstacles.add(obstacle);
        }

        ball.pos = new Vector2i(startPos);
        int biggestX = 0;
        int biggestY = 0;
        walls = new ArrayList<>();
        for (int i = 0; i < level.getWallPoints().size(); i++)
        {
            Vector2i wallPosA = new Vector2i(level.getWallPoints().get(i).get(0).multiply(screenTranslateValue));
            Vector2i wallPosB = new Vector2i(level.getWallPoints().get(i).get(1).multiply(screenTranslateValue));

            Wall tempWall = new Wall(wallPosA, wallPosB);
            walls.add(tempWall);
            if (wallPosB.x > biggestX)
            {
                biggestX = wallPosB.x;
            }
            if (wallPosB.y > biggestY)
            {
                biggestY = wallPosB.y;
            }
        }
        gameSize = new Vector2i(biggestX,biggestY);

        goalPoint = new Vector2i(level.getGoalPoint().multiply(screenTranslateValue));

        levelLoaded = true;
    }
}
