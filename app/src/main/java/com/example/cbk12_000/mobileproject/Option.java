package com.example.cbk12_000.mobileproject;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class Option extends AppCompatActivity {

    private EditText editText;
    private Spinner wall;
    private Spinner ball;
    private SharedPreferences sharedPreferences;

    private String[] color = {"BLACK", "BLUE", "CYAN", "GREEN", "MAGENTA", "RED", "YELLOW"};

    /**
     * Called when the activity is created. Sets up the view for the options menu
     * @param savedInstanceState The saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option);
        sharedPreferences = getSharedPreferences("Name", MODE_PRIVATE);

        String name = sharedPreferences.getString("name", "");
        int wallPos = sharedPreferences.getInt("wallColor", 0);
        int ballPos = sharedPreferences.getInt("ballColor", 0);


        editText = findViewById(R.id.option_name);

        wall = findViewById(R.id.wallSpinner);
        ball = findViewById(R.id.ballSpinner);

        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(this, R.layout.spinner_layout, color);

        wall.setAdapter(adapter);
        ball.setAdapter(adapter);

        wall.setSelection(wallPos);
        ball.setSelection(ballPos);

        if(!name.equals(""))
        {
            editText.setText(name);
        }
    }


    /**
     * Saves all changes. Used with the save button
     * @param view The save button
     */
    public void save(View view)
    {
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("name", editText.getText().toString());
        editor.putInt("ballColor", ball.getSelectedItemPosition());
        editor.putInt("wallColor", wall.getSelectedItemPosition());
        //System.out.println("ball chosee: " + ball.getSelectedItemPosition()+ "  wall chosen: "+  wall.getSelectedItemPosition());
        editor.apply();
        finish();
    }

    /**
     * Disregards all changes. Used with the cancel button
     * @param view The cancel button
     */
    public void cancel(View view)
    {
        finish();
    }

    /**
     * Transforms the color codes we use to androids color codes
     *
     * @param color Our color code
     * @return Androids color codes
     */
    static public int transform(int color)
    {
        int colorCode;
        switch(color)
        {
            case 0: colorCode = Color.BLACK; break;
            case 1: colorCode = Color.BLUE; break;
            case 2: colorCode = Color.CYAN; break;
            case 3: colorCode = Color.GREEN; break;
            case 4: colorCode = Color.MAGENTA; break;
            case 5: colorCode = Color.RED; break;
            case 6: colorCode = Color.YELLOW; break;
            default: colorCode = Color.BLACK; break;
        }
        return colorCode;
    }

}
