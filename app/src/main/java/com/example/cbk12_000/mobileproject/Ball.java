package com.example.cbk12_000.mobileproject;

import android.graphics.Paint;

import com.example.cbk12_000.mobileproject.Vector2D.Vector2d;
import com.example.cbk12_000.mobileproject.Vector2D.Vector2i;


/**
 * Ball class for the ball in the game.
 */
class Ball {
    /**
     * Position of the ball in the game
     */
    public Vector2i pos;
    /**
     * The ball's position in the last frame
     */
    public Vector2i oldPos;

    /**
     * The speed of the ball
     */
    public Vector2d speed;

    /**
     * The radius of the ball
     */
    public int radius;

    /**
     * The paint used to draw the ball
     */
    public Paint paint;

    private double MAX_SPEED = 1000.0;

    /**
     * Constructor to create the ball. Position and speed is set to 0, 0
     *
     * @param paint The paint used to draw the ball
     */
    public Ball(Paint paint)
    {
        this.paint = paint;

        pos = new Vector2i(0, 0);
        speed = new Vector2d(0, 0);
        oldPos = pos;
    }

    /**
     * Updates the speed of the ball. Ball can not surpass max speed. Acceleration will not be applied
     * if it's NaN.
     *
     * @see #MAX_SPEED
     * @param acceleration The acceleration force acting on the ball
     * @param dt The delta time of the update loop
     */
    public void updateSpeed(Vector2d acceleration, double dt)
    {
        if(Double.isNaN(acceleration.x) || Double.isNaN(acceleration.y))
        {
            return;
        }


        speed = speed.add(acceleration.multiply(dt));

        if(speed.absSqrt() > MAX_SPEED * MAX_SPEED)
        {
            speed = speed.normalized().multiply(MAX_SPEED);
        }
    }

    /**
     * Updates the position using the current speed
     *
     * @param dt The delta time of the update loop
     */
    public void updatePos(double dt) {
        oldPos = pos;
        pos = pos.add(speed.multiply(dt));
    }
}
