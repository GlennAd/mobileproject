package com.example.cbk12_000.mobileproject;


import android.content.Context;
import android.util.Log;

import com.example.cbk12_000.mobileproject.Vector2D.Vector2d;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;

class LevelLoader {
    private ArrayList<Level> levels;


    /**
     * Goes through every file in the raw folder and stores the information from all the level
     * files into level class objects, stored in the levellist which calls the function in the first
     * place. Gets the name of the level from the file name. Makes sure the amount of parameters
     * for each line is correct before attempting to save.
     *
     * Credit for lines 36, 37 and 41 goes to https://stackoverflow.com/a/6540269
     *
     * @param context App context needed for InputStream
     */
    LevelLoader(Context context) {
        InputStream stream;

        Field[] fields = R.raw.class.getFields();
        levels = new ArrayList<Level>(){};

        for (Field field : fields) {
            Log.i("Level: ", field.getName());
            int resourceID = 0;
            try {
                resourceID = field.getInt(field);
            } catch (IllegalAccessException e) {
                Log.e("LEVEL_LOADER", "Error Loading Levels: " + e.getStackTrace().toString());
            }
            Log.i("Resource: ", "" + resourceID);
            stream = context.getResources().openRawResource(resourceID);

            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

            String data = "";


            levels.add(new Level());
            levels.get(levels.size() - 1).setName(field.getName().replace("_", " "));
            if (stream != null) {
                try {
                    while ((data = reader.readLine()) != null) {

                        String[] tempData = data.split(" ");
                        int values = 1;
                        ArrayList<Vector2d> tempFloat = new ArrayList<Vector2d>() {
                        };

                        if (tempData[0].equals("W")) {
                            values = 2;
                        } else if (tempData[0].equals("C") || tempData[0].equals("L")) {
                            values = 0;
                        }
                        for (int i = 0; i < values; i++) {
                            Vector2d tempVec = new Vector2d(Double.valueOf(tempData[i + i + 1]), Double.valueOf(tempData[i + i + 2]));

                            tempFloat.add(tempVec);
                        }

                        switch (tempData[0]) {
                            case "W":
                                if (tempFloat.size() == 2) {
                                    levels.get(levels.size() - 1).addWallPoints(tempFloat);
                                } else {
                                    Log.i("wall", "Wrong amount of arguments to create wall (need 4)");
                                }
                                break;
                            case "D":
                                if (tempFloat.size() == 1) {
                                    levels.get(levels.size() - 1).addDangerPoints(tempFloat.get(0));
                                } else {
                                    Log.i("danger", "Wrong amount of arguments to create hazard (need 2)");
                                }

                                break;
                            case "G":
                                if (tempFloat.size() == 1) {
                                    levels.get(levels.size() - 1).setGoalPoint(tempFloat.get(0));
                                } else {
                                    Log.i("goal", "Wrong amount of arguments to create goal point (need 2)");
                                }

                                break;
                            case "S":
                                if (tempFloat.size() == 1) {
                                    levels.get(levels.size() - 1).setStartPoint(tempFloat.get(0));
                                } else {
                                    Log.i("start", "Wrong amount of arguments to create start point (need 2)");
                                }

                                break;
                            default:
                                break;

                        }
                    }
                    stream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * gets the list of levels
     *
     * @return a list of all levels
     */
    public ArrayList<Level> getLevels()
    {
        return levels;
    }

}
