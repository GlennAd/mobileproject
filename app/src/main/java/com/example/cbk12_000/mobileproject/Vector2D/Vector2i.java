package com.example.cbk12_000.mobileproject.Vector2D;

/*
A class for vector math with ints
 */
public class Vector2i {
    /**
     * The x component of the vector
     */
    public int x;
    /**
     * The y component of the vector
     */
    public int y;

    /**
     * Creates an empty vector without the components initialized
     */
    public Vector2i()
    {

    }

    /**
     * Creates a new vector
     * @param x The X component of the vector
     * @param y The Y component of the vector
     */
    public Vector2i(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * Constructor that converts a Vector2d into vector2i form
     *
     * @param vec The vector2d
     */
    public Vector2i(Vector2d vec)
    {
        x = (int)vec.x;
        y = (int)vec.y;
    }

    /**
     * Adds this vector with another
     *
     * @param vec The vector to add
     * @return The added vector
     */
    public Vector2i add(Vector2i vec)
    {
        return new Vector2i(x + vec.x, y + vec.y);
    }

    /**
     * Adds this vector with vector2d
     *
     * @param vec The vector to add
     * @return The added vector
     */
    public Vector2i add(Vector2d vec) {
        return new Vector2i(x + (int)vec.x, y + (int)vec.y);
    }

    /**
     * Subtracts a vector from this one
     * @param vec The vector to subtract from this one
     * @return The subtracted vector
     */
    public  Vector2i sub(Vector2i vec)
    {
        return new Vector2i(x - vec.x, y - vec.y);
    }

    /**
     * Calculates the absolute value of the vector.
     * Should use absSqrt if possible.
     *
     * @see #absSqrt()
     * @return The absolute of the vector
     */
    public double abs()
    {
        return Math.sqrt((x * x) + (y * y));
    }

    /**
     * Calculates the squared absolute value. This is a much lighter
     * operation then abs() and should be used instead where possible.
     *
     * @see #abs()
     * @return The absolute squared of the vector
     */
    public int absSqrt()
    {
        return (x * x) + (y * y);
    }

    /**
     * Interpolates the vector
     *
     * @param dt The interpolation value
     * @param target The target point
     * @return A vector interpolated between this one and target using dt
     */
    public Vector2d interpolate(float dt, Vector2i target)
    {
        Vector2d vec = new Vector2d();

        vec.x = (1.0 - dt)* vec.x + dt * vec.x;
        vec.y = (1.0 - dt)* vec.y + dt * vec.y;

        return vec;
    }

    /**
     * Creates a string based on the vector in the form "X: x, Y: y"
     *
     * @return The string
     */
    @Override
    public String toString()
    {
        return  "X: " + x + ", Y: " + y;
    }
}
