package com.example.cbk12_000.mobileproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

/**
 * The first activity
 */
public class GameMenu extends AppCompatActivity {
    /**
     * initializes the Database and the ads
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Database.getInstance().login(this);
        LevelList.getInstance().init(this);
        setContentView(R.layout.activity_game_menu);
        // This id is generalized for testing apps.
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");
        AdRequest adRequest = new AdRequest.Builder().build();


        AdView adView = findViewById(R.id.adView);


        adView.loadAd(adRequest);

        AdView adView2 = findViewById(R.id.adView2);


        adView2.loadAd(adRequest);

    }

    /**
     * The on click function that starts the level selection activity that starts a game
     * @param view
     */
    public void start(View view)
    {
        Intent intent = new Intent(this, LevelSelection.class);
        intent.putExtra("StartOrHighscore", true);
        startActivity(intent);

    }
    /**
     * The on click function that starts the level selection activity for high score.
     * @param view
     */
    public void highScore(View view)
    {
        Intent intent = new Intent(this, LevelSelection.class);
        intent.putExtra("StartOrHighscore", false);
        startActivity(intent);
    }

    /**
     * the on click function to start the option.
     * @param view
     */
    public void option(View view)
    {
        Intent intent = new Intent(this, Option.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Database.getInstance().onDestroy();
    }
}
