package com.example.cbk12_000.mobileproject;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

/**
 * The main game activity
 */
public class GameActivity extends AppCompatActivity {

    private RotationSensorListener rotationSensorListener;
    private Sensor rotationSensor;

    private SensorManager sensorManager;

    private String TAG = "GAME_ACTIVITY";

    /**
     * Sets up the activity, starts the game view, sets up the sensors and
     * turn off phone rotations
     *
     * @param savedInstanceState The saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GameView view = new GameView(this);
        setContentView(view);


        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);

        rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        rotationSensorListener = new RotationSensorListener(view);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    /**
     * Does nothing special
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * Resumes the rotations sensor
     */
    @Override
    protected void onResume() {
        super.onResume();

        sensorManager.registerListener(rotationSensorListener, rotationSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    /**
     * Pauses the rotation sensor
     */
    @Override
    protected void onPause() {
        super.onPause();

        sensorManager.unregisterListener(rotationSensorListener, rotationSensor);
    }
}
