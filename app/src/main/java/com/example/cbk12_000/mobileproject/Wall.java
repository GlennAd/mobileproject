package com.example.cbk12_000.mobileproject;

import com.example.cbk12_000.mobileproject.Vector2D.Vector2i;

class Wall {
    /**
     * The upper left point of the wall
     */
    public Vector2i startingPoint;
    /**
     * The lower right point of the wall
     */
    public Vector2i endPoint;

    /**
     * The width of the wall
     */
    public int width;
    /**
     * The height of the wall
     */
    public int height;

    /**
     * Constructor that creates a wall from 2 data points
     *
     * @param startingPoint The upper left part of the wall
     * @param endPoint The lower right part of the wall
     */
    public Wall(Vector2i startingPoint, Vector2i endPoint)
    {
        this.startingPoint = startingPoint;
        this.endPoint = endPoint;

        width = Math.abs(endPoint.x - startingPoint.x);
        height = Math.abs(endPoint.y - startingPoint.y);


    }

    /**
     * Checks if the wall intersects with the ball
     * @param ball The ball to check for intersection
     * @return True if it intersects or false if it don't
     */
    public boolean intersects(Ball ball)
    {
        Vector2i circleDistance = new Vector2i();

        circleDistance.x = Math.abs(ball.pos.x - (startingPoint.x + (width / 2)));
        circleDistance.y = Math.abs(ball.pos.y - (startingPoint.y + (height / 2)));


        if (circleDistance.x > (width/2.0 + ball.radius)) { return false; }
        if (circleDistance.y > (height/2.0 + ball.radius)) { return false; }


        if (circleDistance.x <= (width/2.0)) { return true; }
        if (circleDistance.y <= (height/2.0)) { return true; }

        int cornerDistance_sq = (circleDistance.x - width/2) * (circleDistance.x - width/2) +
                (circleDistance.y - height/2) * (circleDistance.y - height/2);

        return (cornerDistance_sq <= (ball.radius * ball.radius));
    }



}
