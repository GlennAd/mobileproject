Authors: 

Benjamin Skinstad

Christoffer Berg Kongsness

Glenn Adrian Nordhus

Hans Emil Eid

Project: 

Our project is based on the popular wooden maze game. An user has to navigate a maze using the phones gyroscope sencors. 
The user tries to navigate as fast as possible trough the maze dodging hazards while playing, competing with other users on a highscore list. 

Code organization:

https://i.imgur.com/lN1RuA8.jpg - Project Diagram

The LevelList and Database classes are singletons due to a need for frequrent access to their information.

Two classes have been specifically created as tools, both working as 2D vectors, one taking ints and one taking doubles. These are stored within the Vector2D folder.

Level format:

Our game has its own level format. The format consists of each line being preface by a letter signifying what the following line represents, helping the program put the information from each line into the correct location.
For now, there are six recognized letters, one of which is to account for a chance in the format and potential deprecated maps. W is short for Wall, followed by four numbers representing coordinates for two opposing corners of the wall (X1, Y1, X2, Y2).
S represents the starting position, holding two numbers representing the coordinates of the start position. G is for the goal point of the level, similarily taking in two numbers to act as coordinates, and D represents each hazard (D for Danger), also taking two numbers. 
C is used to comment the file if necessary, the line being ignored completely once the C is seen, and L, which used to separate the file into levels and hold the name of the level, is now ignored the same way as C. The name is now gotten from the file name, where underscores are
converted to spaces.

A converter program exists to convert bitmap-esque maps into the level format. This C++ program, stored in the Converter subfolder of the project folder, gets the Input.txt file in the project folder, reads a name by the user, and exports the new level into a new file within the raw folder of the application.

The format of the Input.txt file, as explained in the Input_File_Format.txt file in the same location, is built upon a bitmap. With 1 representing walls and 0 representing open spaces, using a bitmap generator you can create the outline of the map easily.
After adding a bitmap (with each 'bit' separated by spaces to be easily readable by the program and users), three other elements can be added manually. Putting a 2 will add an obstacle in that location. 3 will add a starting point, and 4 will add a goal point.
For simplicity in the program, you're also required to add two numbers on the top line of the file, which are the width and heights of the map in 'bits'.

Linter warnings:

-A newer version of firebase is available; upgrading to the latest version was not a necessity.

-Target SDK attribute is not targeting the latest version; upgrading to the latest version removed some functionality.

-Hardcoded Text; default strings are used in debugging and are not supposed to be shown to users.

-TextView Internationalization; text internationalization not needed as the text in question is an integer

-Nested Layout Weights; changing this caused the menu to look different than intended

-Unused Resources; because the level files are located using their assinged location integer rather than the actual path (e.g. R.raw.level), Android Studio is unable to predict their usage.

-Missing Support for Firebase App Indexing; App does not need to be indexable by Google Search.

Issues: 

-Although there are issues when running the code on an emulator, it usually runs fine on phones. One of these issues is phasing through walls when flipping the phone rapidly and repeatedly.

-Corner collison is buggy, and you might on occasion glitch through walls.

-An issue we were unable to identify the cause of causes the player to occasionally get put back into the level select screen upon hitting an obstacle.

