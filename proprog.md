### Students:  
- Glenn Adrian Nordhus  
- Christoffer Berg Kongsness  
- Hans Emil Eid  
- Benjamin Normann Skinstad  

### Repos:
[Mobile Project](https://bitbucket.org/christofferkongsness/mobileproject/src/master/)

### Java with Android SDK

Android made it easy to design and create Graphical User Interfaces and linking it up with the rest of our code. Android also does the memory management for you, which allows focus on other features, but does also mean the garbage collector might give us performance drops.

The language is quite nice in the start, doing a lot of the things we had to do in C++ for us, however as time went on we noticed that the language was limiting because of its lack of flexibility. One example of this, is the template system. We wanted to create mathematical vectors in 2D space, but we could not find any libraries that supported this. We created our own, deciding that we needed support for int and double precision vectors. This would have been easy to template in C++, but Java did not let us use standard mathematical expressions with generic types, so expressions like T + T was not allowed. The language did not let us do this to ensure we did not do illegal operations if we tried templating something else than numbers or strings as the generic type. This handholding made templating useless for our use case and we had to implement two vector classes with the same functionality.
Java is nice to use for large, simple projects, but struggles when faced with non traditional solutions.

### Process and Communication Systems

We did eXtreme programming and split the project in four separate parts. We worked in the same room and communicated primarily verbally. This made our communication a lot faster than with any other means, made it faster to delegate tasks and let us focus on our taks. Since we were in the same room, we could use each other as rubber duckies.  
However, since we worked on seperate parts of the project, we couldn't contribute much to each others part because getting up to speed with their code would take longer than just letting fix it themselves.

### Version Control

We used git with bitbucket. We started using master, and split the project into several branches once we had a base product going. The branches were dedicated to the different features we were working on and was merged into master when they were complete.  
We did not feel there was any point in using tickets for a project this size and the fact that we worked in the same room only made it even more useless. If we had worked on a larger project and were spending more time working alone, tickets would have been far more useful. Version control was not used, as we did not find any use for it.

### Programming Style

We loosely followed Google's style: https://google.github.io/styleguide/javaguide.html  
We never agreed to a particular style, however Android Studio uses this style by default so we followed its advice. We did however, consciously follow the JavaDoc standard for class and function documentation.

### Libraries

We used Android based libraries like Google Play and Firebase. We had some issues using Firebase as we had several different versions of Firebase according to android studio, even though we only added one version to our project. This did not stop us from compiling but we had to spend time trying to find out where this problem occurred without success.

### Professionalism

We started the project 2 days before the project presentation, because the project presentation was moved forward by one day. That made us lose 1/3 of the planned timeslot. Although we admittedly should have set aside more time to work on this project, the fact of the matter is that because of this late rescheduling we lost a large portion of our time. The reason for why we scheduled so little time for this project was due to a focus on our bachelor, and were told that this subject would not interfere significantly with the bachelor assignment.

Our commit messages also varied in professionalism and usefulness, from actually helpful to relatively useless ("changes", "stuff"). A more professional approach to commits would have been to use a proper documentation standard such as smart commits, or prefixing each commit with which tasks we were working on at the time.

### Use of Code Review

We should have used code reviews to a greater extent than we did. Our code reviews were informal processes of quickly looking over each others code and making sure it was compatible. We did, as a group, use Linter warnings to look through our project for issues with syntax and looked at the warnings and errors.

# Individual Discussion

## Glenn Adrian Nordhus  

My individual discussion can be found here: https://bitbucket.org/christofferkongsness/mobileproject/src/master/GlennAdrianIndividual.pdf

## Christoffer Berg Kongsness  
my individual discussion can be found here: https://bitbucket.org/christofferkongsness/mobileproject/src/master/Proprog%20Christoffer%20kongsness.pdf
## Hans Emil Eid  
My individual discussion can be found here: https://bitbucket.org/christofferkongsness/mobileproject/src/master/HansEmilEidIndividual.pdf

## Benjamin Normann Skinstad  
### Good

https://pastebin.com/e56WBpc7
The code I will be talking about is in this bitbucket repo, in the CustomListner.java, the code is also pasted in the pastebin link above. 

The code is easy to read, the structure of the code makes it easy to get a good overview of what's where. The document contains almost no extra documentation, but due to self explaining function and variable names, it is extremely easy to understand the logic of this file. Personally I would have added a bit more documentation in the Datasnapshop for loop at line 50. 

The code seems to be easy to expand on, the functions are compact, and everything is a part of its relevant function. 


### Bad

Note: the code in question is a part of our bachelor project. Mariuz said he could fix you access if you needed it. It is to much relevant text to paste into pastebin. 

The LaserGattlingGun function is started in a file called castpart.cs. There no documentation or comments explaining how this function works in its function call. 

if(spellstate == 25) { LasterGatlingGun(); } 

This 45 item long if check contains almost no documentation of its functions. 

When following the LasterGatlingGun to its declaration, Visual studio tells me its is defined at line 1469 in the castpart.cs file. This function contains on comments or documentation. The function is almost completely similar to every other functions in the script. Every function could be made into one or two lines of code if their programmer would have made an generalized aiming function instead of pasting the aiming code into every function that requires it. The only difference between the ability functions if the script it calls, and in this instance the LasterGatlingGun function calls the RequestLasterGatlingGun function in the LasterGatlingGunSpell script. This script does not contain any documentation for any of the functions in it. 

The code used in RequestLasterGatlingGun is also pasted in most of the other abilities, again, the programmer would probably benefit from creating a more generalized function to be used in here instead. The function here sets up a networking hashtable containing information that has been passed through the function calls. Note, there is only one variable passed into the hashtable that has been passed through the function calls, the rest is generated before the hashtable is passed into the networking function. 

This hashtable is passed as an argument to and RPC function. The RPC function is also in the castpart.cs file. The function takes out information stored in the hashtable, this information is then used the same way as it was in the long if-check i talked about in the beginning. The number used for the LasterGatlingGun in this switch is 25, this is the same number it had in the if-check. Meaning it is a waste to put the number we get from the if-check into a hashtable, only to take the information out from the hashtable has use it in the switch. Note, the programmer mixed switches and if checks, it would be more consistent to only use on type. 

The switch, now run the code in case 25, and another RPC function is called, this time it’s called  “LaserGattlingGunRPC”, with the hashtable as parameters. 

The LaserGattlingGunRPC function is a part of the LaserGattlingGunSpell script. This is, as far as i know, the final script. I am not entirely sure what it does, because there are no documentation explaining said function. It seems to create the spell, fire it, and sync it over the network. 
During our bachelor we spent a lot of time trying to find this ability in the game, and understand how it was used in the program. The LaserGattlingGun spell is implemented the same way as every other spell used in the game, but for some reason it seems like it’s never used. There is no documentation, nor comments that told us that the function was not yet properly implemented in the game. Because there was no official documentation regarding the abilities scripts, we had to manually check each of the 45 abilities in the if-check to see if they were active or not. All of this work could have been avoided if the programmers would have added a comment saying if the function was active or not. 

The code itself was terrible organized, with functions jumping back and forth between scripts, making it harder than necessary to debug. I question the optimization of the code as well, because i don’t think it’s efficient to traverse through scripts making changes to variables, only to restore said variables to the same value they had to begin with, and then to be used in another function in the first script again. The code does what its suppose to do, but the added complexity, and inefficiently can slow the game down. It also makes it hard to make expansions and changes to the code if the programmer desires to do so in the future. This code would benefit from refactoring.  


### Refactored

Link to the original code:
https://pastebin.com/jM70eE0r

Link to the refactored code: 
https://pastebin.com/LeWmxt88

In the refactored code I removed all hard coded variables. I collected all variables used throughout the program and sorted them in the beginning of main. All of the variables is grouped to match their circle for easier changes. I changed the order of the function calls I use to create my circles to match one and other, making it so that all the variables is sat in the same place for the different circles. Since the pattern is the same, it is now easier to both debug the circles, and to create new ones. 

I changed all of the variable names from one letter variables into something more describing. Where needed I added an relevant prefix to variables names, this is to help the reader understand where the variable is relevant. 

I added a few comments to make it easier to see where one should add more circles. I felt the rest of the program was self explanatory. 

If the program was of a larger size, it might have been beneficial to figure out how to generalize the circle generation a bit more, and put everything in functioncalls instead. 


### Comments on professionalism in programming


It doesn’t matter if you are the best programmer in the world if no one else can understand your code. It will be impossible for others to use your code, impossible to expand on it, and when the original programmer is no longer working at that job, no one will know how it works, forcing the company to rewrite the code. 
In a professional setting, a programmer should always consider how the code he is writing will look to someone new. He should make his logic as clear as possible, and include as much documentation is needed to make someone new understand it. 

A programmer should know his own skill limits, but always strive to improve. He should never think he is done learning in the world of programming. If the programmer is unsure about a topic, he should be open about this, and try to learn it with the help of others. If someone on the team of the programmer is struggling with a topic, he should do his best in helping them understand it. 

A programmer should communicate with his team as often as necessary. This is so his team leader can keep track of how everyone's doing, and potentially change up the approach if needed. If a programmer manages to document his progress the team leader can more easy make estimates throughout the project and in new projects. The team leader can also show this to potential customers when they ask for estimated development times.  




