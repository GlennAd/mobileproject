#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

void input(std::vector<std::vector<char>>* map)
{
	std::ifstream file;
	file.open("../../input.txt");

	int sizeX;
	int sizeY;
	file >> sizeX >> sizeY;
	for (size_t i = 0; i < sizeX; i++)
	{

		std::vector<char> tempMap;
		for (size_t j = 0; j < sizeY; j++)
		{
			char a;
			file >> a;
			tempMap.push_back(a);
		}
		map->push_back(tempMap);
	}

	file.close();
}

void output(std::vector<std::vector<float>> corners, float startX, float startY, float goalX, float goalY, std::vector<std::vector<float>> obstacles, std::string name)
{
	std::ofstream file;
	file.open("../../app/src/main/res/raw/" + name);


	for (size_t i = 0; i < corners.size(); i++)
	{
		file << "W ";
		for (size_t j = 0; j < 4; j++)
		{
			file << corners.at(i).at(j) << " ";
		}
		file << "\n";
	}
	file << "S " << startX << " " << startY << "\n";
	file << "G " << goalX << " " << goalY << "\n";
	for (size_t i = 0; i < obstacles.size(); i++)
	{
		file << "D ";
		for (size_t j = 0; j < 2; j++)
		{
			file << obstacles.at(i).at(j) << " ";
		}
		file << "\n";
	}
	file.close();
}

std::vector<std::vector<int>> findSquare(std::vector<std::vector<char>>* map, int x, int y)
{
	std::vector<std::vector<int>> positions;

	for (size_t i = x; i < map->size(); i++)
	{
		std::vector<int> pos;
		if (map->at(i).at(y) == '1')
		{

			pos.push_back(i);
			pos.push_back(y);
			positions.push_back(pos);
		}
		else
		{
			break;
		}
	}
	return positions;
}

void convert()
{
	std::vector<std::vector<char>> map;
	float convertX;
	float convertY;
	input(&map);
	convertX = 100.f / map.size();
	convertY = 100.f / map.at(0).size();
	float startPosX = 20;
	float startPosY = 20;
	float goalPosX = 10;
	float goalPosY = 10;
	std::vector<std::vector<float>> obstacles;

	std::string name;

	std::cout << "Level name:\n";
	std::getline(std::cin, name);
	std::transform(name.begin(),name.end(),name.begin(),::tolower);

	for (size_t i = 0; i < name.length(); i++)
	{
		if (name.at(i) == ' ')
		{
			name.replace(i,1,"_");
		}
	}

	std::vector<std::vector<std::vector<int>>> rectangles;
	for (size_t i = 0; i < map.size(); i++)
	{
		for (size_t j = 0; j < map.at(i).size(); j++)
		{
			if (i > 0)
			{
				if (map.at(i).at(j) == '1' && map.at(i - 1).at(j) != '1')
				{

					std::vector<std::vector<int>> positions = findSquare(&map, i, j);

					rectangles.push_back(positions);


				}
			}
			else
			{
				std::vector<std::vector<int>> positions = findSquare(&map, i, j);

				rectangles.push_back(positions);
			}
			if (map.at(i).at(j) == '2')
			{
				std::vector<float> tempVec;
				tempVec.push_back((i * convertX) + (convertX / 2));
				tempVec.push_back((j * convertY) + (convertY / 2));
				obstacles.push_back(tempVec);
			}
			if (map.at(i).at(j) == '3')
			{
				startPosX = (i * convertX) + (convertX / 2);
				startPosY = (j * convertY) + (convertY / 2);
			}
			if (map.at(i).at(j) == '4')
			{
				goalPosX = (i * convertX) + (convertX / 2);
				goalPosY = (j * convertY) + (convertY / 2);
			}

		}
	}

	std::vector<std::vector<float>> cornerPositions;
	for (size_t i = 0; i < rectangles.size(); i++)
	{

		std::vector<float> positionTemp;
		float firstCornerX = rectangles.at(i).at(0).at(0) * convertX;
		float firstCornerY = rectangles.at(i).at(0).at(1) * convertY;
		float secondCornerX = (rectangles.at(i).at(rectangles.at(i).size() - 1).at(0) * convertX) + convertX;
		float secondCornerY = (rectangles.at(i).at(rectangles.at(i).size() - 1).at(1) * convertY) + convertY;
		positionTemp.push_back(firstCornerX);
		positionTemp.push_back(firstCornerY);
		positionTemp.push_back(secondCornerX);
		positionTemp.push_back(secondCornerY);
		cornerPositions.push_back(positionTemp);
	}
	output(cornerPositions,startPosX,startPosY,goalPosX,goalPosY,obstacles,name);
}

int main()
{
	convert();

	return 0;
}